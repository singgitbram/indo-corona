import React, { useEffect } from 'react';
import { StyleSheet, Text, View, Image, FlatList, ActivityIndicator } from 'react-native';
import { useSelector, useDispatch } from 'react-redux'
import { getAllCountry } from '../store/actions/covidAction'
import { Entypo } from '@expo/vector-icons';
import CountryCard from '../components/CountryCard'

export default function HomeScreen(props) {
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getAllCountry())
    }, [])

    function thousandFormat(num) {
        return num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    };

    const { userName } = useSelector(state => state.userLoginReducer)
    const { globalData } = useSelector(state => state.dataCovidReducer)
    const { allCountry } = useSelector(state => state.dataCovidReducer)

    if (!globalData || !allCountry) {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <ActivityIndicator size="large" color="#4B0082" />
            </View>
        )
    }

    return (
        < View style={styles.container}>
            <Text style={{ alignSelf: 'flex-start', paddingLeft: 10, marginBottom: 10, paddingTop: 10 }}>Halo, {userName} <Entypo name="heart" size={16} color="pink" /></Text>
            <View style={styles.globalBox}>
                <Text style={{ borderBottomWidth: 2 }}>Data Covid19 Dunia</Text>
                <Text>Total Cases: <Text style={{ color: 'blue' }}>{thousandFormat(globalData.TotalConfirmed)}</Text></Text>
                <Text>Total Deaths: <Text style={{ color: 'red' }}>{thousandFormat(globalData.TotalDeaths)}</Text></Text>
                <Text>Total Recovered: <Text style={{ color: 'green' }}>{thousandFormat(globalData.TotalRecovered)}</Text></Text>
            </View>
            <Text>List Negara (sorted):</Text>
            <Text style={{fontStyle:'italic', color:'grey'}}>Klik untuk melihat berita di negara tsb</Text>
            <View>
                <FlatList
                    data={allCountry}
                    renderItem={({ item, index }) => <CountryCard country={item} idx={index} navigation={props.navigation}/>}
                    keyExtractor={(key, index) => index.toString()}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#E6E6FA',
        alignItems: 'center',
        display: 'flex',
    },
    globalBox: {
        backgroundColor: 'white',
        borderRadius: 10,
        marginBottom: 10,
        borderColor: "#800080",
        borderWidth: 1,
        elevation: 5,
        padding: 5
    }
})
