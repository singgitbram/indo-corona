import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, FlatList, ActivityIndicator } from 'react-native';
import { useSelector, useDispatch } from 'react-redux'
import { getNews } from '../store/actions/covidAction'
import { Entypo } from '@expo/vector-icons';
import NewsCard from '../components/NewsCard'

export default function NewsScreen(props) {
    const dispatch = useDispatch()

    useEffect(() => {
        if (!props.route.params) {
            setNegara('indonesia')
            dispatch(getNews('indonesia'))
        } else {
            setNegara(props.route.params.pencarian)
            dispatch(getNews(props.route.params.pencarian))
        }
    }, [])

    const [negara, setNegara] = useState(``)
    const { userName } = useSelector(state => state.userLoginReducer)
    const { news } = useSelector(state => state.dataCovidReducer)

    if (!news) {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <ActivityIndicator size="large" color="#4B0082" />
            </View>
        )
    }

    return (
        < View style={styles.container}>
            <Text style={{ alignSelf: 'flex-start', paddingLeft: 10, marginBottom: 10, paddingTop: 10 }}>Halo, {userName} <Entypo name="heart" size={16} color="pink" /></Text>
            <Text>Berita Covid19 di {negara}</Text>
            <View>
                <FlatList
                    data={news}
                    renderItem={({ item, index }) => <NewsCard news={item} idx={index} />}
                    keyExtractor={(key, index) => index.toString()}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#E6E6FA',
        alignItems: 'center',
        display: 'flex',
    },

})
