import React, { useEffect } from 'react';
import { StyleSheet, Text, View, FlatList, ActivityIndicator } from 'react-native';
import { useSelector, useDispatch } from 'react-redux'
import { getAllProvinsi, getIndonesia } from '../store/actions/covidAction'
import { Entypo } from '@expo/vector-icons';
import ProvinsiCard from '../components/ProvinsiCard'

export default function ProvinsiScreen() {
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getAllProvinsi())
        dispatch(getIndonesia())
    }, [])

    const { userName } = useSelector(state => state.userLoginReducer)
    const { indonesiaData } = useSelector(state => state.dataCovidReducer)
    const { allProvinsi } = useSelector(state => state.dataCovidReducer)

    if (!indonesiaData || !allProvinsi) {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <ActivityIndicator size="large" color="#4B0082" />
            </View>
        )
    }

    return (
        < View style={styles.container}>
            <Text style={{ alignSelf: 'flex-start', paddingLeft: 10, marginBottom: 10, paddingTop: 10 }}>Halo, {userName} <Entypo name="heart" size={16} color="pink" /></Text>
            <View style={styles.globalBox}>
                <Text style={{ borderBottomWidth: 2 }}>Data Covid19 Indonesia</Text>
                <Text>Total Cases: <Text style={{ color: 'blue' }}>{indonesiaData.positif}</Text></Text>
                <Text>Total Deaths: <Text style={{ color: 'red' }}>{indonesiaData.meninggal}</Text></Text>
                <Text>Total Recovered: <Text style={{ color: 'green' }}>{indonesiaData.sembuh}</Text></Text>
            </View>
            <Text>List Provinsi (sorted):</Text>
            <View>
                <FlatList
                    data={allProvinsi}
                    renderItem={({ item, index }) => <ProvinsiCard provinsi={item} idx={index} />}
                    keyExtractor={(key, index) => index.toString()}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#E6E6FA',
        alignItems: 'center',
        display: 'flex',
    },
    globalBox: {
        backgroundColor: 'white',
        borderRadius: 10,
        marginBottom: 10,
        borderColor: "#800080",
        borderWidth: 1,
        elevation: 5,
        padding: 5
    }
})
