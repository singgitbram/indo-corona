import React, { useState } from 'react';
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity } from 'react-native';
import { useDispatch } from 'react-redux'
import { setLogin } from '../store/actions/loginAction'
import { Entypo } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';

export default function LoginScreen(props) {
    const dispatch = useDispatch();
    const [userName, setUserName] = useState('')
    const [password, setPassword] = useState('')
    const [isError, setIsError] = useState(false)

    const handleLogin = () => {
        if (password === '1234') {
            dispatch(setLogin({ userName: userName }))
            props.navigation.push('TabsScreen')
        } else {
            setIsError(true)
        }
    }

    return (
        <View style={styles.container}>
            <Image source={require('../images/hati.png')} style={{ width: 300, height: 100, marginTop: -100 }} />
            <Text style={{ marginTop: 5, fontSize: 25 }}>Login</Text>
            <View style={styles.inputText}>
                <FontAwesome style={{ marginRight: 5, marginLeft: 5 }} name="user" size={24} color="black" />
                <TextInput onChangeText={userName => setUserName(userName)} placeholder="Input your username" style={{ fontSize: 13 }}></TextInput>
            </View>
            <View style={styles.inputText}>
                <Entypo style={{ marginRight: 5 }} name="key" size={24} color="black" />
                <TextInput onChangeText={password => setPassword(password)} secureTextEntry={true} placeholder="Input your password" style={{ fontSize: 13 }}></TextInput>
            </View>
            <View>
                <Text style={{ color: 'grey', fontStyle: 'italic' }}>*Note: insert password 1234 for test</Text>
            </View>
            <Text style={isError ? styles.errorText : styles.hiddenErrorText}>Password Salah</Text>
            <TouchableOpacity style={styles.buttonMasuk} onPress={() => handleLogin()}>
                <Text style={{ backgroundColor: "#9370DB", color: "white", paddingHorizontal: 20, paddingVertical: 5, borderRadius: 200 }}>Masuk</Text>
            </TouchableOpacity>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#E6E6FA',
        alignItems: 'center',
        justifyContent: 'center',
        display: 'flex',
    },
    text: {
        fontSize: 30, color: '#04063c'
    },
    inputText: {
        marginTop: 20,
        width: 200,
        height: 30,
        paddingHorizontal: 10,
        borderBottomWidth: 1,
        borderColor: "#003366",
        flexDirection: "row",
        marginBottom: 5
    },
    buttonMasuk: {
        marginTop: 10,
        height: 30,
        alignItems: 'center',
    },
    buttonDaftar: {
        marginTop: 15,
        height: 30,
        alignItems: 'center',
    },
    errorText: {
        color: 'red',
        textAlign: 'center',
        marginBottom: 1,
    },
    hiddenErrorText: {
        color: 'transparent',
        textAlign: 'center',
        marginBottom: 1,
    }
});