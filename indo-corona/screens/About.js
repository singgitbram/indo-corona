import React from 'react';
import { Linking } from 'react-native'
import { FontAwesome } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import { StyleSheet, Text, View, Image } from 'react-native';
export default function AboutScreen() {

    return (
        <View style={styles.container}>
            <Text style={{ marginTop: 15, fontSize: 30, color: "#003366", marginTop: 0, fontWeight: "bold" }}>Tentang Saya</Text>
            <Image source={require('../images/Singgit_Bramantha.jpg')} style={{
                width: 100,
                height: 100,
                borderRadius: 300,
                marginTop: 10,
                borderColor: "#800080",
                borderWidth: 1
            }} />
            <Text style={{ marginTop: 15, fontSize: 25, color: "#003366", fontWeight: 'bold' }}>Singgit Bramantha</Text>
            <Text style={{ marginTop: 5, fontSize: 15, color: "#9370DB" }}>React Native Developer</Text>
            <View style={styles.boxPorto}>
                <Text style={{ borderBottomWidth: 1, paddingLeft: 5 }}>Portofolio</Text>
                <View style={styles.listPorto}>
                    <View style={styles.detilPorto}>
                        <AntDesign name="github" size={30} color="#9370DB" />
                        <Text onPress={() => Linking.openURL('https://github.com/SinggitBram')} style={{ marginLeft: 10 }}>SinggitBram</Text>
                    </View>
                    <View style={styles.detilPorto}>
                        <AntDesign name="gitlab" size={30} color="#9370DB" />
                        <Text onPress={() => Linking.openURL('https://gitlab.com/singgitbram')} style={{ marginLeft: 10 }}>singgitbram</Text>
                    </View>
                </View>
            </View>
            <View style={styles.boxKontak}>
                <Text style={{ borderBottomWidth: 1, paddingLeft: 5, marginBottom: 5 }}>Hubungi Saya</Text>
                <View style={styles.listKontak}>
                    <View style={styles.detilKontak}>
                        <FontAwesome name="facebook-square" size={30} color="#9370DB" />
                        <Text onPress={() => Linking.openURL('https://www.facebook.com/singgit.bramantha')} style={{ marginLeft: 10 }}>singgit.bramantha</Text>
                    </View>
                    <View style={styles.detilKontak}>
                        <AntDesign name="instagram" size={30} color="#9370DB" />
                        <Text onPress={() => Linking.openURL('https://www.instagram.com/singgitbramantha/')} style={{ marginLeft: 10 }}>@singgit_bramantha</Text>
                    </View>
                    <View style={styles.detilKontak}>
                        <AntDesign name="twitter" size={30} color="#9370DB" />
                        <Text onPress={() => Linking.openURL('https://twitter.com/domzdewa')} style={{ marginLeft: 10 }}>@singgit</Text>
                    </View>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#E6E6FA',
        alignItems: 'center',
        justifyContent: 'center',
        display: 'flex',
    },
    boxPorto: {
        display: 'flex',
        backgroundColor: "white",
        height: 100,
        width: 320,
        marginBottom: 10,
        borderRadius: 10,
        marginTop: 10,
        borderColor: "#800080",
        borderWidth: 1,
        elevation: 5
    },
    boxKontak: {
        display: 'flex',
        backgroundColor: "white",
        height: 150,
        width: 320,
        borderRadius: 10,
        borderColor: "#800080",
        borderWidth: 1,
        elevation: 5
    },
    listKontak: {
        display: 'flex',
        alignItems: 'center',
    },
    detilKontak: {
        display: 'flex',
        flexDirection: 'row',
        marginVertical: 5
    },
    detilPorto: {
        display: 'flex',
        alignItems: 'center',
        marginTop: 10
    },
    listPorto: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around'
    }
});
