import React from "react";
import { StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import LoginScreen from './screens/LoginScreen'
import HomeScreen from './screens/HomeScreen'
import ProvinsiScreen from './screens/ProvinsiScreen'
import NewsScreen from './screens/NewsScreen'
import AboutScreen from './screens/About'
import { Provider } from "react-redux";
import store from "./store";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const Stack = createStackNavigator()
const Tabs = createBottomTabNavigator()

const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name="HomeScreen" component={HomeScreen} options={{ title: 'Home',tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="home" color={color} size={size} />
          ), }} />
    <Tabs.Screen name="ProvinsiScreen" component={ProvinsiScreen} options={{ title: 'Indonesia' ,tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="flag" color={color} size={size} />
          ),}} />
    <Tabs.Screen name="NewsScreen" component={NewsScreen} options={{ title: 'News',tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="newspaper" color={color} size={size} />
          ), }} />
    <Tabs.Screen name="AboutScreen" component={AboutScreen} options={{ title: 'About' ,tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="account-box" color={color} size={size} />
          ),}} />
  </Tabs.Navigator>
)

export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Login Screen">
          <Stack.Screen name="LoginScreen" component={LoginScreen} options={{ headerShown: false }} />
          <Stack.Screen name="TabsScreen" component={TabsScreen} options={{title:'Halaman Utama'}}/>
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
