import React from 'react';
import { StyleSheet, Text, View, Image, Linking } from 'react-native';

export default function NewsCard(props) {

    return (
        <View style={styles.boxNews}>
            <Image style={{ width: '100%', height: 200, marginTop: 5, borderRadius: 10 }} source={{ uri: props.news.urlToImage }} />
            <Text style={{ fontWeight: 'bold', paddingHorizontal: 5, fontSize: 20 }}>{props.news.title}</Text>
            <Text onPress={() => Linking.openURL(props.news.url)} style={{}} style={{ alignSelf: 'flex-start', color: 'blue', paddingLeft: 5 }}>Link</Text>
            <Text style={{ paddingHorizontal: 5 }}>{props.news.description}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    boxNews: {
        flex: 1,
        display: 'flex',
        backgroundColor: "white",
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 10,
        borderColor: "#800080",
        borderWidth: 1,
        elevation: 5,
        alignItems: 'center',
        marginVertical: 5,
        marginHorizontal: 10
    },
})