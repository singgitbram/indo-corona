import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default function ProvinsiCard(props) {

    function thousandFormat(num) {
        return num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    };

    return (
        <View style={styles.boxProvinsi}>
            <Text>{props.idx + 1}.</Text>
            <Text>Provinsi: {props.provinsi.Provinsi}</Text>
            <Text>Total Cases: <Text style={{ color: 'blue' }}>{thousandFormat(props.provinsi.Kasus_Posi)}</Text></Text>
            <Text>Total Deaths: <Text style={{ color: 'red' }}>{thousandFormat(props.provinsi.Kasus_Meni)}</Text></Text>
            <Text>Total Recovered: <Text style={{ color: 'green' }}>{thousandFormat(props.provinsi.Kasus_Semb)}</Text></Text>
        </View>
    )
}

const styles = StyleSheet.create({
    boxProvinsi: {
        display: 'flex',
        backgroundColor: "white",
        height: 120,
        width: 320,
        borderRadius: 10,
        borderColor: "#800080",
        borderWidth: 1,
        elevation: 5,
        alignItems: 'center',
        marginVertical: 5,
    },
})