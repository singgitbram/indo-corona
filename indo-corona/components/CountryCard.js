import React from 'react';
import { StyleSheet, Text, View, Image, TouchableWithoutFeedback } from 'react-native';

export default function CountryCard(props) {

    function thousandFormat(num) {
        return num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    };

    return (
        <TouchableWithoutFeedback onPress={() => props.navigation.push('TabsScreen', { screen: 'NewsScreen', params: { pencarian: props.country.Country } })}>
            <View style={styles.boxCountry}>
                <Text>{props.idx + 1}.</Text>
                <Image style={{ width: 40, height: 40 }} source={{ uri: `https://www.countryflags.io/${props.country.CountryCode}/flat/64.png` }} />
                <Text>Country: {props.country.Country}</Text>
                <Text>Total Cases: <Text style={{ color: 'blue' }}>{thousandFormat(props.country.TotalConfirmed)}</Text></Text>
                <Text>Total Deaths: <Text style={{ color: 'red' }}>{thousandFormat(props.country.TotalDeaths)}</Text></Text>
                <Text>Total Recovered: <Text style={{ color: 'green' }}>{thousandFormat(props.country.TotalRecovered)}</Text></Text>
            </View>
        </TouchableWithoutFeedback>
    )
}

const styles = StyleSheet.create({
    boxCountry: {
        display: 'flex',
        backgroundColor: "white",
        height: 150,
        width: 320,
        borderRadius: 10,
        borderColor: "#800080",
        borderWidth: 1,
        elevation: 5,
        alignItems: 'center',
        marginVertical: 5,
    },
})