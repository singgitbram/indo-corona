import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import userLoginReducer from "./reducers/userLoginReducer"
import dataCovidReducer from "./reducers/dataCovidReducer";

const reducers = combineReducers({userLoginReducer,dataCovidReducer})

const store = createStore(reducers, applyMiddleware(thunk));

export default store;
