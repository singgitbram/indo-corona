import axios from 'axios';

export const getAllCountry = () => {
    return (dispatch) => {
        return axios({
            method: 'get',
            url: `https://api.covid19api.com/summary`
        })
            .then(({ data }) => {
                let countrySort = data.Countries.sort(function (a, b) {
                    return b.TotalConfirmed - a.TotalConfirmed;
                });
                data.Countries = countrySort
                dispatch({
                    type: "GETALLCOUNTRY",
                    payload: data
                })
            })
    }
}

export const getAllProvinsi = () => {
    return (dispatch) => {
        return axios({
            method: 'get',
            url: `https://api.kawalcorona.com/indonesia/provinsi`
        })
            .then(({ data }) => {
                let newData = []
                data.forEach(element => {
                    newData.push(element.attributes)
                });
                let newDataSort = newData.sort(function (a, b) {
                    return b.Kasus_Posi - a.Kasus_Posi;
                });
                dispatch({
                    type: "GETALLPROVINSI",
                    payload: newDataSort
                })
            })
    }
}

export const getIndonesia = () => {
    return (dispatch) => {
        return axios({
            method: 'get',
            url: `https://api.kawalcorona.com/indonesia`
        })
            .then(({ data }) => {
                dispatch({
                    type: "GETINDODATA",
                    payload: data[0]
                })
            })
    }
}

export const getNews = (params) => {
    return (dispatch) => {
        return axios({
            method: 'get',
            url: `https://newsapi.org/v2/everything?q=covid ${params}&apiKey=94f9e87a0bff4eabb5940f49b4b7819e`
        })
            .then(({ data }) => {
                dispatch({
                    type: "GETNEWS",
                    payload: data.articles
                })
            })
    }
}