const initialState = {
    globalData: null,
    allCountry: [],
    allProvinsi: [],
    indonesiaData: null,
    news : []
}

function dataCovidReducer(state = initialState, action) {
    if (action.type === "GETALLCOUNTRY") {
        return { ...state, globalData: action.payload.Global, allCountry: action.payload.Countries };
    }
    if (action.type === "GETALLPROVINSI") {
        return { ...state, allProvinsi: action.payload };
    }
    if (action.type === "GETINDODATA") {
        return { ...state, indonesiaData: action.payload };
    }
    if (action.type === "GETNEWS") {
        return { ...state, news: action.payload };
    }
    return state;
}

export default dataCovidReducer;