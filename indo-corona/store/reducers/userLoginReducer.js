const initialState = {
    isLogin: false,
    userName: null
}

function userLoginReducer(state = initialState, action) {
    switch (action.type) {
        case "SETLOGIN":
            return { ...state, isLogin: action.payload.isLogin, userName: action.payload.userName }
        default:
            return state;
    }
}

export default userLoginReducer